package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    int countByTechnologyName(String technology);

    @Query("SELECT t " +
            "FROM Team t " +
            "JOIN t.users u " +
            "WHERE t.technology.name = :technology " +
            "GROUP BY t " +
            "HAVING count(u) < :devsNumber")
    List<Team> findByDevsNumberLessThanAndTechnology(long devsNumber, String technology);

    @Modifying
    @Query(value = "UPDATE teams team SET name = " +
            "concat(team.name, '_', p.name, '_', tech.name) " +
            "FROM projects p, technologies tech " +
            "WHERE team.name = :team " +
            "AND team.project_id = p.id " +
            "AND team.technology_id = tech.id", nativeQuery = true)
    void normalizeName(String team);

    Optional<Team> findByName(String name);
}
