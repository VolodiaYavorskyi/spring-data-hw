package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        teamRepository.findByDevsNumberLessThanAndTechnology(devsNumber, oldTechnologyName)
                .forEach(team -> {
                    Technology tech = team.getTechnology();
                    tech.setName(newTechnologyName);
                    technologyRepository.save(tech);
                });
    }

    @Transactional
    public void normalizeName(String hipsters) {
        teamRepository.normalizeName(hipsters);
    }
}
