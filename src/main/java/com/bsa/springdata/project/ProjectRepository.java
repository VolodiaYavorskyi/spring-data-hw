package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query("SELECT count(DISTINCT p) FROM Project p JOIN p.teams t JOIN t.users u JOIN u.roles r WHERE r.name = :role")
    int countByUsersWithRole(String role);

    @Query(value = "SELECT p.* " +
            "FROM projects p " +
            "JOIN teams team ON team.project_id = p.id " +
            "JOIN technologies tech ON team.technology_id = tech.id " +
            "JOIN users u ON u.team_id = team.id " +
            "WHERE tech.name = :technology " +
            "GROUP BY p.id " +
            "ORDER BY count(u.id) DESC " +
            "LIMIT 5", nativeQuery = true)
    List<Project> findTop5ByTechnology(String technology);

    @Query(value = "SELECT p.* " +
            "FROM projects p " +
            "JOIN teams team ON team.project_id = p.id " +
            "JOIN users u ON u.team_id = team.id " +
            "GROUP BY p.id " +
            "ORDER BY count(team.id) DESC, count(u.id) DESC, p.name DESC " +
            "LIMIT 1", nativeQuery = true)
    Optional<Project> findTheBiggestByTeamsThanDevelopersThanName();

    @Query(value = "SELECT p.name, count(DISTINCT team.id) AS teamsNumber, " +
            "count(DISTINCT u.id) AS developersNumber, " +
            "string_agg(DISTINCT tech.name, ',' ORDER BY tech.name DESC) AS technologies " +
            "FROM projects p " +
            "JOIN teams team ON team.project_id = p.id " +
            "JOIN users u ON u.team_id = team.id " +
            "JOIN technologies tech ON team.technology_id = tech.id " +
            "GROUP BY p.id " +
            "ORDER BY p.name ASC", nativeQuery = true)
    List<ProjectSummaryDto> getSummary();
}