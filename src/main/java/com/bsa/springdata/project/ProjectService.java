package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        return projectRepository.findTop5ByTechnology(technology)
                .stream()
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        return projectRepository.findTheBiggestByTeamsThanDevelopersThanName().map(ProjectDto::fromEntity);
    }

    public List<ProjectSummaryDto> getSummary() {
        return projectRepository.getSummary();
    }

    public int getCountWithRole(String role) {
        return projectRepository.countByUsersWithRole(role);
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        Project project = new Project();
        project.setName(createProjectRequest.getProjectName());
        project.setDescription(createProjectRequest.getProjectDescription());

        Technology technology = new Technology();
        technology.setName(createProjectRequest.getTech());
        technology.setDescription(createProjectRequest.getTechDescription());
        technology.setLink(createProjectRequest.getTechLink());

        Team team = new Team();
        team.setRoom(createProjectRequest.getTeamRoom());
        team.setName(createProjectRequest.getTeamName());
        team.setArea(createProjectRequest.getTeamArea());
        team.setProject(project);
        team.setTechnology(technology);


        UUID res = projectRepository.save(project).getId();
        technologyRepository.save(technology);
        teamRepository.save(team);

        return res;
    }
}
