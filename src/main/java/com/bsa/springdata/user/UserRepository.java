package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findByLastNameContainingIgnoreCase(String lastName, Pageable pageable);

    @Query("SELECT u FROM User u WHERE u.office.city = :city ORDER BY u.lastName ASC")
    List<User> findByCityOrderByLastNameAsc(String city);

    List<User> findByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    @Query("SELECT u FROM User u WHERE u.team.room = :room AND u.office.city = :city ORDER BY u.lastName ASC")
    List<User> findByRoomAndCityOrderByLastNameAsc(String city, String room);

    int deleteByExperienceLessThan(int experience);
}
