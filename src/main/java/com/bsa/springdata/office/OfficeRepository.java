package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query("SELECT DISTINCT o FROM Office o JOIN o.users u WHERE u.team.technology.name = :technology")
    List<Office> findByTechnology(String technology);

    @Modifying
    @Query(value = "UPDATE offices o SET address = :newAddress " +
            "FROM (SELECT DISTINCT office_id " +
            "FROM users u JOIN offices o ON u.office_id = o.id " +
            "WHERE o.address = :oldAddress " +
            "GROUP BY o.id, u.id " +
            "HAVING count(u.id) > 0) AS sub", nativeQuery = true)
    void updateAddress(String oldAddress, String newAddress);

    List<Office> findByAddress(String address);
}
